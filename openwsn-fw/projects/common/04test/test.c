/**
\brief This project is useful for testing the mote in communication.
It would be under porting for Contiki & Riot

Functions implemented:
- open comunication
- sending packet
- information parsed and put somewhere (to be decided)

 */

#include "board.h"
#include "opendefs.h"
#include "openserial.h"
#include "radio.h"

//=========================== defines =========================================

#define LENGTH_PACKET   125+LENGTH_CRC /// maximum length = 127 bytes

//=========================== variables =======================================

typedef struct {
   app_state_t          app_state;
   uint8_t              flag;
   uint8_t              packet[LENGTH_PACKET];
   uint8_t              packet_len;
    int8_t              rxpk_rssi;
   uint8_t              rxpk_lqi;
   bool                 rxpk_crc;
   uint8_t              channel;
   uint8_t              outputOrInput;
} app_vars_t;

app_vars_t app_vars;

//=========================== prototypes ======================================

//=========================== main ============================================

int mote_main(void){

	// clear local variables
	memset(&app_vars,0,sizeof(app_vars_t));

	// initialize board
	board_init();
	scheduler_init();
	openserial_init();
	idmanager_init();



	return 0;
}
