#include "opendefs.h"
#include "sixtop.h"
#include "openserial.h"
#include "openqueue.h"
#include "neighbors.h"
#include "IEEE802154E.h"
#include "IEEE802154.h"
#include "IEEE802154_security.h"
#include "iphc.h"
#include "packetfunctions.h"
#include "openrandom.h"
#include "scheduler.h"
#include "opentimers.h"
#include "debugpins.h"
#include "detas.h"
#include "schedule.h"
#include "idmanager.h"


//=========================== variables =======================================
PRAGMA(pack(1));
typedef struct {
	 uint8_t		 evenorodd;
	 uint8_t		 scheduleCase;
	 uint16_t		 startingTime1;
	 uint16_t        startingTime2;
	 uint8_t		 QS;
	 uint8_t		 theta;
	 uint8_t 		 rank;
	 uint8_t		 detasSerial;
} debugdetas_scheduleVars_t;
PRAGMA(pack());

detas_scheduleVars_t detas_scheduleVars;
detas_vars_t detas_vars;


//=========================== prototypes ======================================
void detas_updateDetasData();
void detas_setResPayloadBigger	(OpenQueueEntry_t* msg);
void detas_setResPayloadNormal	(OpenQueueEntry_t* msg);
void detas_setResPayloadSmaller	(OpenQueueEntry_t* msg);
//=========================== public ==========================================



void detas_init()
{

	detas_vars.localPktNr			 = 2;
	detas_scheduleVars.my16baddress	 = idmanager_getMyID(ADDR_16B);

	if (neighbors_getMyDAGrank()==0)
	{
		detas_vars.scheduleState = 0;
		detas_scheduleVars.scheduleCase=0;
	}
	else
	{
		detas_vars.scheduleState=1;
	}
}


void detas_initRoot()
{
	detas_vars.localPktNr 				= 0;
	detas_vars.scheduleState			= 0;
	detas_scheduleVars.detasSerial 		= 0;
	schedule_updateSlot();
}

void detas_incrementSerial()
{
	if(detas_vars.globalPktNr == 2 )
	{
		detas_scheduleVars.detasSerial=30;
		return;
	}
	detas_scheduleVars.detasSerial = 	detas_scheduleVars.detasSerial +1;
}



uint8_t detas_getSerial()
{
	return detas_scheduleVars.detasSerial;
}


bool detas_updateSerial(OpenQueueEntry_t* msg)
{
	if (detas_scheduleVars.detasSerial < msg->payload[1])
	{
		detas_scheduleVars.detasSerial = msg->payload[1];
		return TRUE; //return TRUE if there was an update of the serial number
	}
	return FALSE;//return FALSE if there if the serial number received was the same
}


void detas_setSchedule()
{
	SetdetasScheduleBuilt(TRUE);
}



void detas_setScheduleDetasState()
{
	schedule_setDetasSchedule	(&detas_scheduleVars, &detas_vars);
}


/**
 This function builds the payload for the REQ command packet
 *
 */
void detas_setReqPayload(OpenQueueEntry_t* msg)
{
	//we reserve 2 bytes for the payload of this packet, one for the Global Packet Number and the other one for the command ID number
	packetfunctions_reserveHeaderSize(msg,sizeof(IEEE154_CMD_REQ)+sizeof(detas_vars.globalPktNr) + sizeof(detas_vars.localPktNr));

	msg->payload[0] 	= 	IEEE154_CMD_REQ;
	msg->payload[1] 	=	detas_vars.globalPktNr;
	msg->payload[2] 	=	detas_vars.localPktNr;
}

/**
 This function builds the payload for the RES command packet
 *
 */
void detas_setResPayload(OpenQueueEntry_t* msg)
{
	if (detas_vars.evenlength == detas_vars.oddlength)
	{
		detas_setResPayloadNormal(msg);
		detas_scheduleVars.scheduleCase 	=	CASENORMAL;
	}
	else
		if (detas_vars.childs[0].globalPktNr > detas_vars.globalPktNr/2)
		{
			detas_setResPayloadBigger(msg);
			detas_scheduleVars.scheduleCase 	=	CASEGREATER;
		}
		else
			if (detas_vars.childs[0].globalPktNr < detas_vars.globalPktNr/2)
			{
				detas_setResPayloadSmaller(msg);
				detas_scheduleVars.scheduleCase 	=	CASESMALLER;
			}
			else
			{
				//this condition should never be verified
				return;
			}

	//if we are here we have sent the RES packet and we need to update the detas state for the neighbors
	uint8_t i;
	for(i=0; i < detas_vars.nrNeighbors;i++)
	{
		setDetasState(detas_vars.childs[i].addr_64b);
	}
}



void detas_setResPayloadBigger(OpenQueueEntry_t* msg)
{
	uint16_t startEvenTime   = 	MAINTENANCESLOTNR;
	uint16_t startOddTime	 =	MAINTENANCESLOTNR;
	uint8_t temp_8b;
	uint8_t i;

	packetfunctions_reserveHeaderSize(msg,1 + 1  + 1 + detas_vars.nrNeighbors*5 + 1);

	// Set the first byte of payload  -  command identifier
	msg->payload[0]   		 =	 	IEEE154_CMD_RES;

	// Set the second byte of payload  -  Nr of neighbors
	msg->payload[1]   		 =	 	detas_vars.nrNeighbors;

	// setting the third byte of the payload  -  Hop counter
	detas_scheduleVars.rank	 = 		0; // this function is to be called from the root so it has rank = 0
	msg->payload[2]			 =		detas_scheduleVars.rank +	1;

	// here we set the payload data for all the neighbors except the first
	// in this case the first neighbour is even scheduled and the rest are odd scheduled
	for(i=0; i < detas_vars.nrNeighbors-1;i++)
	{

		//inserting the last 2 bytes the address
		msg->payload[3 + i*5]   = 	detas_vars.childs[i+1].addr_64b.addr_64b[6];
		msg->payload[4 + i*5]   = 	detas_vars.childs[i+1].addr_64b.addr_64b[7];

		if (detas_vars.childs[i+1].evenOrOdd == 0)
		{
			break;
		}
		//inserting byte 3
		temp_8b 			 =	0;
		temp_8b             |= 	detas_vars.childs[i+1].evenOrOdd           << 0; // The first byte is the type of schedule even or odd
		temp_8b             |= 	CASENORMAL      		                   << 1; // the next 3 bytes determine the schedule case for the particular neighbor

		msg->payload[5 + i*5]   = 	temp_8b;

		//inserting byte 4-5 the start time
		msg->payload[6 + i*5]   = 	(startOddTime    	 &	 0xff);
		msg->payload[7 + i*5]   = 	(startOddTime/256	 &	 0xff);
		startOddTime 		   +=	detas_vars.childs[i].globalPktNr*2;

	}

	//The last data to be inserted to the payload are those of the first neighbor of the list
	i=detas_vars.nrNeighbors-1;

	//inserting the first 2 bytes the address
	msg->payload[3 + i*5]   = 	detas_vars.childs[0].addr_64b.addr_64b[6];
	msg->payload[4 + i*5]   = 	detas_vars.childs[0].addr_64b.addr_64b[7];


	//inserting byte 3
	temp_8b = 0;
	temp_8b             |= 	detas_vars.childs[0].evenOrOdd         	<< 0 ; // The first byte is the type of schedule even or odd
	temp_8b             |=	CASEGREATER  		   					<< 1 ; // the next 3 bytes determine the schedule case for the particular neighbor

	msg->payload[5 + i*5]   =	 temp_8b;


	//inserting byte 4-5 the start time
	msg->payload[6 + i*5]   =	(startEvenTime    	 & 		0xff);
	msg->payload[7 + i*5]   = 	(startEvenTime/256	 &		0xff);

	// inserting byte 6 for
	msg->payload[8 + i*5]   = 	detas_vars.globalPktNr;

}

void detas_setResPayloadNormal(OpenQueueEntry_t* msg){

	//define variables
	uint16_t 	startEvenTime 	= 	MAINTENANCESLOTNR;
	uint16_t 	startOddTime	=	MAINTENANCESLOTNR;
	uint8_t 	temp_8b;
	uint8_t 	i;

	packetfunctions_reserveHeaderSize(msg,1+ 1 + 1 + detas_vars.nrNeighbors*5 );

	// Set the first byte of payload  -  command identifier
	msg->payload[0]   		 =	 	IEEE154_CMD_RES;

	// Set the second byte of payload  -  Nr of neighbors
	msg->payload[1]   		 =	 	detas_vars.nrNeighbors;

	// setting the third byte of the payload  -  Hop counter
	detas_scheduleVars.rank	 = 		0; 	// this function is to be called from the root so it has rank = 0
	msg->payload[2]			 =		detas_scheduleVars.rank +1;


	  // here we insert the payload for each neighbor in normal mode
	  for(i=0; i <  detas_vars.nrNeighbors;i++)
	  {
		  //inserting the first 2 bytes the address
		  msg->payload[3 + i*5]   = 	detas_vars.childs[i].addr_64b.addr_64b[6];
		  msg->payload[4 + i*5]   = 	detas_vars.childs[i].addr_64b.addr_64b[7];

		  //inserting byte 3 evenOrOdd bit + scheduleCase
		  temp_8b	  =	 	0;
		  temp_8b    |= 	detas_vars.childs[i].evenOrOdd   	 		 << 0 ; // The first byte is the type of schedule even or odd
		  temp_8b    |=		CASENORMAL 				       		         << 1 ; // the next 3 bytes determine the schedule case for the particular neighbor

		  msg->payload[5 + i*5]   = 	temp_8b;

		  //inserting byte 4-5 the start time
		  if(detas_vars.childs[i].evenOrOdd == 0)
		  {
			  msg->payload[6 + i*5]   =		(startEvenTime   		 &		0xff);
			  msg->payload[7 + i*5]   = 	(startEvenTime/256		 & 		0xff);
			  startEvenTime += detas_vars.childs[i].globalPktNr * 2;

		  }
		  else if(detas_vars.childs[i].evenOrOdd	==	1 )
		  {
			  msg->payload[6 + i*5]   = 	(startOddTime   	 & 		0xff);
			  msg->payload[7 + i*5]   = 	(startOddTime/256	 & 		0xff);
			  startOddTime += detas_vars.childs[i].globalPktNr * 2;

		  }
	  }
}


void detas_setResPayloadSmaller(OpenQueueEntry_t* msg){

	uint16_t startEvenTime = 	MAINTENANCESLOTNR;
	uint16_t startOddTime	 =	MAINTENANCESLOTNR;
	uint8_t temp_8b;
	uint8_t j 	= 	0;
	uint8_t i;
	uint8_t theta;

	packetfunctions_reserveHeaderSize(msg, 1 + 1 + 1 + (detas_vars.nrNeighbors)*5 + 3 );

	if (detas_vars.evenlength > detas_vars.oddlength)
	{
		theta 	=  (detas_vars.evenlength-detas_vars.oddlength)/2;
		startEvenTime  += 2*(detas_vars.childs[0].globalPktNr - theta);  //this will be the starting time of the second evenscheduled child

		msg->payload[0]   = 		IEEE154_CMD_RES;
		msg->payload[1]   =		detas_vars.nrNeighbors;
		detas_scheduleVars.rank	 = 		0; // this function is to be called from the root so it has rank = 0
		msg->payload[2]			 =		detas_scheduleVars.rank +1;

		for(i = 0; i < detas_vars.nrNeighbors - 1; i++)
		{	//Skipping the first element of the evenlist which is going to be added last
			if (j == 0 ){j++;}//Skipping the first element of the oddlist which is going to be added last

			//inserting the first 2 bytes the address
			msg->payload[3 + i*5]  	=	detas_vars.childs[j].addr_64b.addr_64b[6]     	;
			msg->payload[4 + i*5]   	= 	detas_vars.childs[j].addr_64b.addr_64b[7]		;

			//inserting byte 3
			temp_8b = 0;
			temp_8b        |=	 	detas_vars.childs[j].evenOrOdd   	      << 0 ; // The first byte is the type of schedule even or odd
			temp_8b        |= 		CASENORMAL 				                  << 1 ; // the next 3 bytes determine the schedule case for the particular neighbor

			msg->payload[5 + i*5]   = 	temp_8b;

			//inserting byte 4-5 the start time
			if(detas_vars.childs[j].evenOrOdd	==	0)
			{
				msg->payload[6 + i*5]   =		(startEvenTime     		& 	0xff);
				msg->payload[7 + i*5]   = 		(startEvenTime/256 		& 	0xff);
				startEvenTime += detas_vars.childs[j].globalPktNr * 2;
			}
			else if(detas_vars.childs[j].evenOrOdd==1 )
			{

				msg->payload[6 + i*5]   = 		(startOddTime    		& 	0xff);
				msg->payload[7 + i*5]   = 		(startOddTime/256 		& 	0xff);
				startOddTime += detas_vars.childs[j].globalPktNr * 2;

			}
			j++;
		}
		startEvenTime = 	MAINTENANCESLOTNR;
		i=detas_vars.nrNeighbors-1;

		//inserting the first 2 bytes the address
		msg->payload[3 + i*5]   = 		detas_vars.childs[0].addr_64b.addr_64b[6]     	;
		msg->payload[4 + i*5]   = 		detas_vars.childs[0].addr_64b.addr_64b[7]		;

		//inserting byte 3
		temp_8b 		 = 		0;
		temp_8b        |= 		detas_vars.childs[0].evenOrOdd         	<< 0 ; // The first byte is the type of schedule even or odd
		temp_8b        |= 		CASESMALLER     						<< 1 ; // the next 3 bytes determine the schedule case for the particular neighbor

		msg->payload[5 + i*5]   = 		temp_8b;

		//inserting byte 4-5 the start time
		msg->payload[6 + i*5]   = 		(startEvenTime     		& 	0xff);
		msg->payload[7 + i*5]   = 		(startEvenTime/256 		& 	0xff);

		// inserting byte 6 for
		msg->payload[8 + i*5]   = 		theta; //theta

		//inserting byte 7-8 the start time
		msg->payload[9 + i*5]   = 		(startOddTime     		&	0xff);
		msg->payload[10 + i*5]   = 		(startOddTime/256 		& 	0xff);
	}

	if (detas_vars.evenlength < detas_vars.oddlength)
	{

		theta = (detas_vars.oddlength-detas_vars.evenlength)/2;
		startOddTime  += 2*(detas_vars.childs[0].globalPktNr - theta);  //this will be the starting time of the second evenscheduled child

		msg->payload[0]   = 	IEEE154_CMD_RES;
		msg->payload[1]   = 	detas_vars.nrNeighbors;
		detas_scheduleVars.rank	 = 		0; // this function is to be called from the root so it has rank = 0
		msg->payload[2]			 =		detas_scheduleVars.rank +1;

		for(i = 0; i < detas_vars.nrNeighbors - 1; i++)
		{
			if (j == 1 ){j++;}//Skipping the first element of the oddlist which is going to be added last

			//inserting the first 2 bytes the address
			msg->payload[3 + i*5]   = 		detas_vars.childs[j].addr_64b.addr_64b[6]		;
			msg->payload[4 + i*5]   = 		detas_vars.childs[j].addr_64b.addr_64b[7] 		;

			//inserting byte 3
			temp_8b = 0;
			temp_8b             	|= 		detas_vars.childs[j].evenOrOdd   	      << 0 ; // The first byte is the type of schedule even or odd
			temp_8b             	|= 		CASENORMAL 				                  << 1 ; // the next 3 bytes determine the schedule case for the particular neighbor

			msg->payload[5 + i*5]   = 		temp_8b;

			//inserting byte 4-5 the start time
			if(detas_vars.childs[j].evenOrOdd==0)
			{
				msg->payload[6 + i*5]   = 		(startEvenTime   		 &	 	0xff);
				msg->payload[7 + i*5]   = 		(startEvenTime/256		 & 		0xff);
				startEvenTime += detas_vars.childs[j].globalPktNr * 2;
			}
			else if(detas_vars.childs[j].evenOrOdd==1 )
			{
				msg->payload[6 + i*5]   = 		(startOddTime   		 & 		0xff);
				msg->payload[7 + i*5]   = 		(startOddTime/256 		 & 		0xff);
				startOddTime += detas_vars.childs[j].globalPktNr * 2;
			}
			j++;
		}

		startOddTime 			 = 		MAINTENANCESLOTNR;
		i=detas_vars.nrNeighbors-1;

		//inserting the first 2 bytes the address
		msg->payload[3 + i*5]   = 		detas_vars.childs[1].addr_64b.addr_64b[6]     	;
		msg->payload[4 + i*5]   = 		detas_vars.childs[1].addr_64b.addr_64b[7]		;

		//inserting byte 3
		temp_8b 				  = 		0;
		temp_8b           	 |= 		detas_vars.childs[1].evenOrOdd        	 	<< 0 ; // The first byte is the type of schedule even or odd
		temp_8b             	 |=			CASESMALLER    								<< 1 ; // the next 3 bytes determine the schedule case for the particular neighbor

		msg->payload[5 + i*5]   = 		temp_8b;

		//inserting byte 4-5 the start time
		msg->payload[6 + i*5]   = 		(startEvenTime     		& 	0xff);
		msg->payload[7 + i*5]   = 		(startEvenTime/256 		& 	0xff);

		// inserting byte 6 for
		msg->payload[8 + i*5]   = 		theta; //theta

		//inserting byte 7-8 the start time
		msg->payload[9 + i*5]   = 		(startOddTime     		& 	0xff);
		msg->payload[10 + i*5]  = 		(startOddTime/256 		& 	0xff);
	}
}



void detas_setForwardResPayload(OpenQueueEntry_t* msg)
{
	uint16_t startingTime;
	uint8_t i;
	uint8_t temp_8b;

	switch (detas_scheduleVars.scheduleCase)
	{
	case CASENORMAL:
	case CASEGREATER:
		//The first byte is reserved for COMMAND ID the second for the  length
		packetfunctions_reserveHeaderSize(msg,1 + 1 + 1 + (detas_vars.nrNeighbors)*5);
		startingTime 		= 		detas_scheduleVars.startingTime1;
		msg->payload[0]   	= 		IEEE154_CMD_RES;
		msg->payload[1]   	= 		detas_vars.nrNeighbors;

		msg->payload[2]	 	=		detas_scheduleVars.rank +1;


		for(i=0;i<=detas_vars.nrNeighbors-1;i++)
		{

			//inserting the first 2 bytes the address
			msg->payload[3 + i*5]   = 	detas_vars.childs[i].addr_64b.addr_64b[6]     	;
			msg->payload[4 + i*5]   = 	detas_vars.childs[i].addr_64b.addr_64b[7] 		;

			//inserting byte 3
			temp_8b 			=	0;
			temp_8b            |= 	detas_scheduleVars.evenorodd  		  		<< 0 ; // The first byte is the type of schedule even or odd
			temp_8b            |= 	CASENORMAL 				          			<< 1 ; // the next 3 bytes determine the schedule case for the particular neighbor

			msg->payload[5 + i*5]   = 	temp_8b;

			msg->payload[6 + i*5]   = 	(startingTime    		&	0xff);
			msg->payload[7 + i*5]   = 	(startingTime/256 		& 	0xff);
			startingTime += detas_vars.childs[i].globalPktNr * 2;

		}
		break;
	case CASESMALLER:
		//The first byte is reserved for COMMAND ID the second for the  length  and one of the schedules may need 8 bytes
		packetfunctions_reserveHeaderSize(msg,1 + 1 + (detas_vars.nrNeighbors)*5 + 3);

		startingTime = 	detas_scheduleVars.startingTime1;

		msg->payload[0]   = 		IEEE154_CMD_RES;
		msg->payload[1]   = 		detas_vars.nrNeighbors;

		msg->payload[2]	 =		detas_scheduleVars.rank +1;

		uint16_t endingTime1 	= 	detas_scheduleVars.startingTime1 + 2*(detas_vars.globalPktNr - detas_scheduleVars.theta);
		uint8_t 	j 				= 	detas_vars.nrNeighbors;
		uint8_t 	evenorodd 		= 	detas_scheduleVars.evenorodd;
		uint8_t 	k	=	0;
		uint16_t startingTime1;
		uint8_t theta;

		for(i=0;i<=detas_vars.nrNeighbors-1;i++)
		{
			if ((startingTime + detas_vars.childs[i].globalPktNr*2 ) > endingTime1)
			{
				// this is the case where we have found the cut neighbor

				if (detas_scheduleVars.evenorodd == 0){ evenorodd = 1;}	// change evenorodd variable from even to odd or viceversa
				if (detas_scheduleVars.evenorodd == 1){ evenorodd = 0;}

				j 	= 	i; 		//save the position in of the cut neighbor
				startingTime1 	=	 startingTime;	//save the startingtime1 for the cut neighbour
				theta 			= 	((startingTime + detas_vars.childs[i].globalPktNr*2 ) - endingTime1)/2; // save Theta for the cut neighbor

				startingTime	= 	detas_scheduleVars.startingTime2;	//get the startingtime2
				i++;		// increment (i) in order to schedule for the next neighbor, the cut neighbor is scheduled last
			}

			if ((startingTime + detas_vars.childs[i].globalPktNr*2 ) == endingTime1)
			{
				// this is the case when there is no need tu have a cut neighbor because endingtime1 equals endtime of a neighbor
				// we need only to change even or odd and the start time
				if (detas_scheduleVars.evenorodd == 0){ evenorodd = 1;}
				if (detas_scheduleVars.evenorodd == 1){ evenorodd = 0;}
				startingTime 	= 	detas_scheduleVars.startingTime2;
			}

			//inserting the first 2 bytes the address
			msg->payload[3 + k*5]   =	detas_vars.childs[i].addr_64b.addr_64b[6]    	;
			msg->payload[4 + k*5]   = 	detas_vars.childs[i].addr_64b.addr_64b[7]		;


			//inserting byte 3
			temp_8b 			 = 	0;
			temp_8b             |= 	detas_scheduleVars.evenorodd	 	      << 0 ; // The first byte is the type of schedule even or odd
			temp_8b             |= 	CASENORMAL 				        	      << 1 ; // the next 3 bytes determine the schedule case for the particular neighbor

			msg->payload[5 + k*5]   = 	temp_8b;

			//inserting byte 4-5 the start time
			msg->payload[6 + k*5]   = 	(startingTime     		& 	0xff);
			msg->payload[7 + k*5]   = 	(startingTime/256 		& 	0xff);

			startingTime 	+= 	detas_vars.childs[i].globalPktNr * 2;
			k++;
		}

		if (j != detas_vars.nrNeighbors)
		{
			//inserting the first 2 bytes the address
			msg->payload[3 + k*5]   = 	detas_vars.childs[j].addr_64b.addr_64b[6]		;
			msg->payload[4 + k*5]   = 	detas_vars.childs[j].addr_64b.addr_64b[7]		;

			//inserting byte 3
			temp_8b 				 = 	0;
			temp_8b             	|= 	detas_scheduleVars.evenorodd	   	      << 0 ; // The first byte is the type of schedule even or odd
			temp_8b             	|= 	CASESMALLER 				              << 1 ; // the next 3 bytes determine the schedule case for the particular neighbor

			msg->payload[5 + k*5]    = 	temp_8b;

			//inserting byte 4-5 the start time1
			msg->payload[6 + k*5]   = 	(startingTime1    	 	& 	0xff);
			msg->payload[7 + k*5]   = 	(startingTime1/256 		& 	0xff);

			//inserting byte 6 theta
			msg->payload[8 + k*5]   	= 	theta;
			//inserting byte 7-8 the start time2
			msg->payload[9 + k*5]   	= 	(startingTime     		& 	0xff);
			msg->payload[10 + k*5]  	= 	(startingTime/256 		& 	0xff);
		}
		break;
	}

	for(i=0; i <= detas_vars.nrNeighbors-1;i++)
	{
		setDetasState(detas_vars.childs[i].addr_64b);
	}
}



void detas_buildEvenOrOddTable(){
	uint8_t evenlength = detas_vars.childs[0].globalPktNr;
	detas_vars.childs[0].evenOrOdd = 0;
	uint8_t oddlength = 0;
	uint8_t i;

	for (i=1;i<MAXNUMNEIGHBORS;i++){
		//the table is sorted so if we find a globalPktNr=0 so will be the rest of them. in this case break the for cycle
		if (detas_vars.childs[i].globalPktNr == 0){break;}

		if(oddlength < evenlength){
			detas_vars.childs[i].evenOrOdd = 1;
			oddlength += detas_vars.childs[i].globalPktNr;
		}
		else{
			detas_vars.childs[i].evenOrOdd = 0;
			evenlength += detas_vars.childs[i].globalPktNr;
		}
	}
	detas_vars.evenlength = evenlength;
	detas_vars.oddlength  = oddlength;
}


void detas_updateDetasData(){

	//Cleaning the detas_vars before updating it from the neigbour's list
	uint8_t i;

	for (i=0;i<MAXNUMNEIGHBORS;i++){
		detas_vars.childs[i].DAGrank 			= DEFAULTDAGRANK;
		detas_vars.childs[i].globalPktNr		= 0;
		detas_vars.childs[i].localPktNr		= 0;
		detas_vars.childs[i].evenOrOdd		= 0;
		detas_vars.nrNeighbors = 0;
	}

	detas_vars.globalPktNr 				= 0;

	//Update detas_vars calling the following function defined in neighbors.c
	neighbors_updateDetasVars( &detas_vars);
}

/**
* This function parses the command packet end find out whether it is a detas command.
* in that case depending on the command it saves the parameters received and returns false.
* if it is not a detas command it returns true and does not free the buffer because the packet is to be forwarded to an upper layer
*
*/
bool detas_processCommand(OpenQueueEntry_t* msg)
{

	uint8_t command;
	bool returnval = TRUE;
	uint8_t globalPktNr;
	uint8_t localPktNr;
	open_addr_t srcAddress;

	// do nothing because if the message length is zero than there is no REQ or RES command
	if(msg->length ==0)
	{
		return returnval;
	}

	command   =     msg->payload[0];

	if (command==IEEE154_CMD_REQ)
	{
		// this is a CMD_REQ we have to find out the source address of this packet and update the global packet number
		globalPktNr		= 	    msg->payload[1];
		localPktNr	   	=		msg->payload[2];
		bool state;

		if(detas_getScheduleState()!= 0 && neighbors_getMyDAGrank() != 0)
		{
			returnval	 		=	 	FALSE;
			openqueue_freePacketBuffer(msg);
			return returnval;
		}

		if(neighbors_isPreferredParent(&msg->l2_nextORpreviousHop))
		{
			returnval	 		=	 	FALSE;
			openqueue_freePacketBuffer(msg);
			return returnval;
		}

		state = neighbors_updatePktNumbers(&msg->l2_nextORpreviousHop, globalPktNr, localPktNr );

		if (state == 3)
		{
			returnval	 		=	 	FALSE;
			openqueue_freePacketBuffer(msg);
			return returnval;
		}

		if (state == FALSE && detas_getScheduleState()== 0)
		{
			sendRES();
		}
		else if (state==1)
		{
			detas_setScheduleState(1);
		}

		// if we get here than we should return a false value
		returnval	 		=	 	FALSE;
		openqueue_freePacketBuffer(msg);
		if (detas_getScheduleState()!= 0 && neighbors_getMyDAGrank()!=0){
			sendREQ(); // after we have stored the data we need to forward a REQ up the network
		}
		return returnval;
	}else
		if (command==IEEE154_CMD_RES)
		{
			if (detas_updateSerial(msg)==FALSE)
			{
				openqueue_freePacketBuffer(msg); //free the buffer from the packet
				return FALSE;
			}
			detas_scheduleVars.detasSerial = msg->payload[1];
			packetfunctions_tossHeader(msg, sizeof(uint8_t));

			returnval = FALSE;
			uint8_t nrofmotes;
			nrofmotes = msg->payload[1];
			uint8_t temprank = msg->payload[2];
			uint8_t j;
			uint8_t i;
			uint16_t moteaddress;
			uint8_t temp_8b = 0;

			if (nrofmotes == 0)
			{
				// if we are in this case there should be a problem because we have received a RES command with payload zero
				openqueue_freePacketBuffer(msg);
				return FALSE;
			}

			for(j = 1; j <= nrofmotes; j++)
			{
				i = (j-1)*5+3;
				moteaddress = msg->payload[i]+256*msg->payload[i+1];

				if (detas_scheduleVars.my16baddress->addr_16b[0] == msg->payload[i] && detas_scheduleVars.my16baddress->addr_16b[1] == msg->payload[i+1] )
				{
					detas_scheduleVars.scheduleCase = 0;
					detas_scheduleVars.evenorodd  = 0;
					detas_scheduleVars.QS = 0;
					detas_scheduleVars.startingTime1 = 0;
					detas_scheduleVars.startingTime2 = 0;
					detas_scheduleVars.theta = 0;
					detas_scheduleVars.rank = 0;

					temp_8b = msg->payload[i+2];
					detas_scheduleVars.evenorodd		=	 (temp_8b >> 0)& 0x01;
					detas_scheduleVars.scheduleCase		=	 (temp_8b >> 1 )& 0x07;

					switch (detas_scheduleVars.scheduleCase)
					{
					case CASENORMAL:
						detas_scheduleVars.startingTime1 	= 	msg->payload[i+3]	+	256*msg->payload[i+4];
						detas_scheduleVars.rank 			=	temprank;
						break;

					case CASEGREATER:
						detas_scheduleVars.startingTime1	=	 msg->payload[i+3]	+	256*msg->payload[i+4];
						detas_scheduleVars.QS 	 			=	 msg->payload[i+5];
						detas_scheduleVars.rank 			=	 temprank;
						break;

					case CASESMALLER:
						detas_scheduleVars.startingTime1 	 = 	msg->payload[i+3]	+	256*msg->payload[i+4];
						detas_scheduleVars.theta 			 = 	msg->payload[i+5];
						detas_scheduleVars.startingTime2 	 = 	msg->payload[i+6]	+	256*msg->payload[i+7];
						detas_scheduleVars.rank 			 =	temprank;
						break;

					default:
						break;
					}

					detas_setScheduleState(2); // after building the schedule we should set the scheduleState to

					openqueue_freePacketBuffer(msg);
					detas_setSchedule();

					res_setRESdelay(j);
				}
			}
			return returnval;
		}
	return returnval;
}


//uint8_t
bool detas_getScheduleState()
{
	return detas_vars.scheduleState;
}


void  	detas_setScheduleState(bool state)
{
	detas_vars.scheduleState = state;
}


/**
\brief Trigger this module to print status information, over serial.

debugPrint_* functions are used by the openserial module to continuously print
status information about several modules in the OpenWSN stack.

\returns TRUE if this function printed something, FALSE otherwise.
*/
bool debugPrint_DetasParameters()
{
	debugdetas_scheduleVars_t output;

	output.evenorodd 		= detas_scheduleVars.evenorodd;
	output.scheduleCase 	= detas_scheduleVars.scheduleCase;
  	output.startingTime1  	= detas_scheduleVars.startingTime1;
  	output.startingTime2   	= detas_scheduleVars.startingTime2;
  	output.QS   			= detas_scheduleVars.QS;
  	output.theta			= detas_scheduleVars.theta;
  	output.rank				= detas_scheduleVars.rank;
  	output.detasSerial		= detas_scheduleVars.detasSerial;
  	openserial_printStatus(STATUS_DETAS,(uint8_t*)&output,sizeof(debugdetas_scheduleVars_t));
  	return TRUE;
}
