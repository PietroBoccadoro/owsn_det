#ifndef __DETAS_H
#define __DETAS_H


#include "opendefs.h"

//#define NUMNEIGHBORS			  10
#define CASESINK		           0
#define CASENORMAL		           1
#define CASEGREATER     		   2
#define CASESMALLER		           3
/**
\addtogroup MAChigh
\{
\addtogroup Schedule
\{
*/
//=========================== define ==========================================

//=========================== typedef =========================================



//=========================== prototypes ======================================

void detas_init();

void detas_initRoot();
//Send functions


// TODO DETAS define a function in detas to build the payload of REQ takes req as parameter and returns true or false

void  detas_setReqPayload(OpenQueueEntry_t* msg);

// TODO DETAS define a function in detas to build the payload of RES takes res as parameter and returns true or false
// TODO DETAS this function is called from a sink node

void detas_setResPayload(OpenQueueEntry_t* msg);


// TODO DETAS define a function in detas to build the payload of RES takes res as parameter and returns true or false
// TODO DETAS This function is called from the generic node after it has received a RES packet from its parents
void detas_setForwardResPayload(OpenQueueEntry_t* msg);

bool detas_getScheduleState();

void detas_setScheduleState(bool state);

//Receive functions

//TODO DETAS if we have received a command packet first we process if it is a detas related command
// this function processes the packet if it is a packet related to detas stores the data, frees the buffer and returns FALSE value in order to pass directly to break statement

bool detas_processCommand(OpenQueueEntry_t* msg);
void detas_incrementSerial();
uint8_t detas_getSerial();
bool detas_updateSerial(OpenQueueEntry_t* msg);
void detas_buildEvenOrOddTable();
void detas_setScheduleDetasState();
/**
\}
\}
*/

#endif
